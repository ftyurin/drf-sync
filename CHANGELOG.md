# Change Log


## 0.6.2 - 2016-08-10

### Changed

- huge query optimization for the sync list view by improving SyncObject.model_name method


## 0.6.1 - 2016-04-18

### Added

- utils.update_or_init_object added
- SYNC_OBJECTS_EXCLUDED setting added


## 0.6.0 - 2016-04-15

### Changed

- user field in api-token-auth success response contains user data with person side.
  Previously user field contained person data.


## 0.5.3 - 2016-03-30

### Fixed

- update_object handles ObjectDoesntExist exception


## 0.5.2 - 2016-03-29

### Added

- utils.py containing get_or_init_object and update_object helper functions


## 0.5.1 - 2016-03-23

### Added

- get_sync_object_serializer_class helper function for creation of base serializer class
  based on model class.


## 0.5.0 - 2015-11-04

### Added

- X-Sync-Modification-Till header returned when GET parameter if specified (even empty).


## 0.4.2 - 2015-10-25

### Fixed

- is_changed implementation


## 0.4.1 - 2015-10-22

### Added

- is_changed property for SyncObject


## 0.4.0 - 2015-10-13

### Added

- Filter parameter "deleted"
- Preserve original state of model instance after loading from DB (is_new,
  original_field_values properties and get_original_field_value method added).

### Changed

- Do not return fields if for deleted object.


## 0.3.0 - 2015-09-18

### Added

- has_save_permission and has_delete_permission methods added to SyncObject
  to protect objects from unauthorized use.


## 0.2.2 - 2015-09-15

### Fixed

- Fix in sync_js_models (invalid JavaScript was generated)


## 0.2.1 - 2015-09-11

### Changed

- sync_js_models generated models extending base Database.Model with "syncable: true",
  instead of extending them from Database.SyncableModel (which doesn't exist).


## 0.2.0 - 2015-09-09

### Added

- sync_js_models management command for generating corresponding JavaScript models
- Call pre_save_polymorphic in SyncObject.model_name if polymorphic_ctype is not set
  in order to be able to serializer model instances before saving them.

### Fixed

- sync.auth.views fixed to use request.data instead of request.DATA


## 0.1.2 - 2015-08-21


## 0.1.1 - 2015-08-20


## 0.1.0 - 2015-08-20
