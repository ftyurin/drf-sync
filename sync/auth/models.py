from django.utils import timezone
from django.conf import settings
from rest_framework.authtoken.models import Token

class AuthToken(Token):
    
    class Meta:
        proxy = True
        
    def get_expires_at(self):
        return self.created + settings.AUTH_TOKEN_EXPIRY
    
    def is_expired(self):
        utc_now = timezone.datetime.utcnow().replace(tzinfo=timezone.utc)
        return self.created < utc_now - settings.AUTH_TOKEN_EXPIRY