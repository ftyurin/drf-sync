from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication

from .models import AuthToken

class ExpiringTokenAuthentication(TokenAuthentication):
    
    model = AuthToken
    
    """
    Make authentication tokens expire in some time
    """
    
    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.select_related('user').get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('User inactive or deleted')

        if token.is_expired():
            raise exceptions.AuthenticationFailed('Token has expired')

        return token.user, token