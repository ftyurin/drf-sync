from django.conf.urls import patterns, url
from .views import (
    Login, Logout, UserDetails, PasswordChange,
    PasswordReset, PasswordResetConfirm, ObtainExpiringAuthToken
)


urlpatterns = patterns(
    '',
    url(r'^login/$', Login.as_view(),
        name='login'),
    url(r'^logout/$', Logout.as_view(),
        name='logout'),
    url(r'^password/change/$', PasswordChange.as_view(),
        name='password-change'),
    url(r'^password/reset/$', PasswordReset.as_view(),
        name='password-reset'),
    url(r'^password/reset/confirm/$', PasswordResetConfirm.as_view(),
        name='password-reset-confirm'),
    url(r'^user/$', UserDetails.as_view(),
        name='user'),
    url(r'^api-token-auth/', ObtainExpiringAuthToken.as_view()),
)
