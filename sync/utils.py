from django.core.exceptions import ObjectDoesNotExist


def get_or_init_object(model, kwargs):
    try:
        return model.objects.get(**kwargs), False
    except model.DoesNotExist:
        pass
    return model(**kwargs), True


def update_object(obj, kwargs):
    updated = False
    for k, v in kwargs.iteritems():
        try:
            if getattr(obj, k) != v:
                setattr(obj, k, v)
                updated = True
        except ObjectDoesNotExist, e:
            setattr(obj, k, v)
            updated = True
    return updated


def update_or_init_object(model, key_kwargs, update_kwargs, save=False):
    obj, new_obj = get_or_init_object(model, key_kwargs)
    updated = update_object(obj, update_kwargs)
    if new_obj or updated and save:
        obj.save()
    return obj, new_obj, updated
