from django.apps import apps
from django.shortcuts import render
from django.utils import timezone, dateparse

from rest_framework import status, viewsets, exceptions
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .auth.authenticators import ExpiringTokenAuthentication
from .models import SyncObject
from .serializers import SyncObjectSerializer


class SyncObjectFilterBackend(object):
    def filter_queryset(self, request, queryset, view):
        queryset = queryset.filter_by_user(request.user)

        since = request.query_params.get('since', None)
        if since is not None:
            since_dt = dateparse.parse_datetime(since)
            if since_dt is None:
                raise exceptions.ParseError('unable to parse since datetime')
            if since_dt.tzinfo is None:
                since_dt = timezone.make_aware(since_dt)
            queryset = queryset.filter(modification_dt__gt=since_dt)

        till = request.query_params.get('till', None)
        if till:
            till_dt = dateparse.parse_datetime(till)
            if till_dt is None:
                raise exceptions.ParseError('unable to parse till datetime')
            if till_dt.tzinfo is None:
                till_dt = timezone.make_aware(till_dt)
            queryset = queryset.filter(modification_dt__lte=till_dt)
        elif till is not None:
            till_dt = timezone.now()
            queryset = queryset.filter(modification_dt__lte=till_dt)
        else:
            till_dt = None

        if till_dt:
            view.headers['X-Sync-Modification-Till'] = till_dt.isoformat()

        model_name = request.query_params.get('model_name', None)
        if model_name is not None:
            # Validate model_name
            try:
                queryset = queryset.instance_of(
                    apps.get_model(*model_name.split('.')))
            except LookupError, e:
                raise exceptions.ParseError(str(e))

        deleted = dict(true=True, false=False).get(
            request.query_params.get('deleted', None), None)
        if deleted is not None:
            queryset = queryset.filter(deleted=deleted)

        return queryset.distinct()


class SyncObjectViewSet(viewsets.ModelViewSet):
    queryset = SyncObject.objects.all()
    serializer_class = SyncObjectSerializer
    filter_backends = (SyncObjectFilterBackend,)
    authentication_classes = (SessionAuthentication, ExpiringTokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def perform_destroy(self, instance):
        if not instance.has_delete_permission(self.request.user):
            raise exceptions.PermissionDenied()
        # FIXME: Should this be moved to SyncObject.delete()?
        instance.deleted = True
        instance.save()

    def perform_create(self, serializer):
        serializer.user = self.request.user # serializer.create uses self.user
        super(SyncObjectViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        serializer.user = self.request.user # serializer.update uses self.user
        super(SyncObjectViewSet, self).perform_update(serializer)
