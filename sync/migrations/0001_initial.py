# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='SyncObject',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('modification_dt', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('polymorphic_ctype', models.ForeignKey(related_name='polymorphic_sync.syncobject_set+', editable=False, to='contenttypes.ContentType', null=True)),
            ],
            options={
                'ordering': ('modification_dt',),
            },
        ),
    ]
