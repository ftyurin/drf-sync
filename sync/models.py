import uuid

from django.conf import settings
from django.db import models

from polymorphic import PolymorphicModel, PolymorphicManager, PolymorphicQuerySet


_sync_object_classes = set()
_sync_objects_excluded = set(getattr(settings, 'SYNC_OBJECTS_EXCLUDED', []))


class SyncObjectManager(PolymorphicManager):
    pass


class SyncObjectQuerySet(PolymorphicQuerySet):
    def filter_by_user(self, user):
        q = None
        for sync_object_class in _sync_object_classes:
            if '{}.{}'.format(
                    sync_object_class._meta.app_label,
                    sync_object_class._meta.object_name) in _sync_objects_excluded:
                continue
            if q is None:
                q = sync_object_class.get_query_by_user(user)
            else:
                q |= sync_object_class.get_query_by_user(user)
        if q is None:
            return self
        return self._clone().filter(q)


class SyncObject(PolymorphicModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    modification_dt = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ('modification_dt',)

    objects = SyncObjectManager(SyncObjectQuerySet)

    @classmethod
    def get_query_by_user(cls, user):
        return models.Q(instance_of=cls)

    def has_save_permission(self, user):
        return True

    def has_delete_permission(self, user):
        return True

    def __unicode__(self):
        return u'[{0} id: {1}]'.format(self.__class__.__name__, self.id)

    def model_name(self):
        if not self.polymorphic_ctype_id:
            self.pre_save_polymorphic()
        ctype = self._get_polymorphic_ctype(self.polymorphic_ctype_id)
        return u'{0}.{1}'.format(
            ctype.app_label, ctype.model
        )

    @classmethod
    def _get_polymorphic_ctype(cls, pk):
        if not hasattr(cls, '_POLYMORPHIC_CTYPES'):
            ctypes = cls.polymorphic_ctype.field.rel.to.objects.all()
            setattr(cls, '_POLYMORPHIC_CTYPES', {
                ct.pk: ct for ct in ctypes
            })
        return cls._POLYMORPHIC_CTYPES[pk]

    @classmethod
    def from_db(cls, db, field_names, values):
        new = super(SyncObject, cls).from_db(db, field_names, values)
        field_values = getattr(new, '_original_field_values', dict())
        field_values.update(dict(zip(field_names, values)))
        new._original_field_values = field_values
        return new

    @property
    def is_new(self):
        '''
        Checks if inctance is newly created of exists in DB.
        '''
        if not self.pk or self._state.adding:
            return True
        return False

    @property
    def original_field_values(self):
        return getattr(self, '_original_field_values', None)

    def get_original_field_value(self, field_name):
        return self._original_field_values[field_name]

    @property
    def is_changed(self):
        original_field_values = self.original_field_values
        if original_field_values is None:
            return False
        for field_name, original_value in original_field_values.iteritems():
            current_value = getattr(self, field_name)
            current_value_type = type(current_value)
            if current_value_type != type(original_value):
                original_value = current_value_type(original_value)
            if current_value != original_value:
                return True
        return False


def handle_sync_object_prepared_signal(sender, **kwargs):
    if issubclass(sender, SyncObject):
        _sync_object_classes.add(sender)

models.signals.class_prepared.connect(handle_sync_object_prepared_signal)
