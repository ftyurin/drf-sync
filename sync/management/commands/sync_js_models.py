import json

from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from sync.models import SyncObject
from sync.serializers import create_model_serializer, SyncObjectSerializer


class Command(BaseCommand):
    help = 'Generates Javascript models'

    def add_arguments(self, parser):
        parser.add_argument('apps', nargs='+', type=str)

    def write(self, text, indent=0, prefix='', postfix='\n'):
        lines = text.splitlines()
        n = len(lines)
        for i, line in enumerate(lines):
            self.stdout.write(
                "    " * indent + (prefix if not i else '') + line,
                ending='\n' if i < n - 1 else '')
        self.stdout.write('', ending=postfix)

    def handle(self, *args, **options):
        self.write('/**')
        self.write(' * The following code was automatically generated using sync_js_models.')
        self.write(' */')
        self.write('define(["database"], function (Database) {')
        self.write('var Models = {', indent=1)
        first = True
        for app_name in options['apps']:
            app_config = apps.get_app_config(app_name)
            app_models = app_config.get_models()
            for model in app_models:
                if SyncObject not in model.__bases__:
                    continue
                if first:
                    first = False
                else:
                    self.write(',')
                self.write('{}: Database.Model.extend({{'.format(model.__name__),
                           indent=2)
                obj = model()
                data = SyncObjectSerializer(obj).data
                data.pop('deleted')
                data.pop('id')
                model_name = data.pop('model_name')
                data.pop('modification_dt')
                self.write('db_store: "{}",'.format(model_name.replace('.', '_')), indent=3)
                self.write('db_schema: "id",', indent=3)
                self.write('syncable: true,', indent=3)
                self.write(json.dumps(data, indent=4), indent=3, prefix='defaults: ')
                self.write('})', indent=2, postfix='')
        self.write('')
        self.write('};', indent=1)
        self.write('return Models;', indent=1)
        self.write('});')
