from django.contrib import admin
from django.core import urlresolvers
from django.utils.translation import ugettext as _

from .models import SyncObject


class BaseSyncObjectAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'modification_dt', 'deleted',
    )
    list_filter = (
        'deleted', 'modification_dt',
    )
    readonly_fields = (
        'id', 'modification_dt',
    )
    search_fields = (
        'id',
    )

    def get_fieldsets(self, *args, **kwargs):
        sync_object_fields = ['id', 'modification_dt', 'deleted']
        fieldset_list = []
        for fieldset in super(BaseSyncObjectAdmin, self).get_fieldsets(*args, **kwargs):
            if fieldset[0] is None:
                fieldset[1]['fields'] = [field for field in fieldset[1]['fields']
                                         if field not in sync_object_fields]
            fieldset_list.append(fieldset)
        fieldset_list.append(
            (
                _('Synchronization options'), 
                {
                    'fields': sync_object_fields,
                },
            )
        )
        return fieldset_list


@admin.register(SyncObject)
class SyncObjectAdmin(BaseSyncObjectAdmin):
    list_display = BaseSyncObjectAdmin.list_display + ('get_model_admin_link',)
    list_filter = BaseSyncObjectAdmin.list_filter + ('polymorphic_ctype',)

    def has_add_permission(self, request):
        return False

    def get_model_admin_url(self, obj):
        try:
            return urlresolvers.reverse(
                'admin:{0}_{1}_change'.format(obj.polymorphic_ctype.app_label,
                                              obj.polymorphic_ctype.model),
                args=(str(obj.id),))
        except urlresolvers.NoReverseMatch:
            return None

    def get_model_admin_link(self, obj):
        url = self.get_model_admin_url(obj)
        if not url:
            return obj.model_name
        return u'<a href="{}">{}.{}</a>'.format(
            url, obj.polymorphic_ctype.app_label, obj.polymorphic_ctype.model)
    get_model_admin_link.allow_tags = True
    get_model_admin_link.short_description = _('model')
