from .views import SyncObjectViewSet

from rest_framework import routers


router = routers.SimpleRouter()
router.register(r'objects', SyncObjectViewSet)


urlpatterns = router.urls
