import uuid
from collections import OrderedDict

from django.apps import apps
from django.db import models

from rest_framework import exceptions, fields, relations, serializers
from rest_framework.serializers import raise_errors_on_nested_writes
from rest_framework.utils import model_meta

from .models import SyncObject


class SyncObjectRelatedField(relations.PrimaryKeyRelatedField):
    def to_representation(self, value):
        # FIXME: Is there a better way to detect UUID?
        try:
            return uuid.UUID(value.pk)
        except:
            pass
        return super(SyncObjectRelatedField, self).to_representation(value)


def get_sync_object_serializer_class(model_class):
    class SOS(serializers.ModelSerializer):
        model_name = serializers.CharField(max_length=201, min_length=3, read_only=True)

        serializer_related_field = SyncObjectRelatedField

        class Meta:
            model = model_class
            exclude = ('polymorphic_ctype',)
            read_only_fields = ('modification_dt', 'deleted',)

        # Override `create` and `update` to check user permissions
        def create(self, validated_data, user=None):
            """
            We have a bit of extra checking around this in order to provide
            descriptive messages when something goes wrong, but this method is
            essentially just:

                return ExampleModel.objects.create(**validated_data)

            If there are many to many fields present on the instance then they
            cannot be set until the model is instantiated, in which case the
            implementation is like so:

                example_relationship = validated_data.pop('example_relationship')
                instance = ExampleModel.objects.create(**validated_data)
                instance.example_relationship = example_relationship
                return instance

            The default implementation also does not handle nested relationships.
            If you want to support writable nested relationships you'll need
            to write an explicit `.create()` method.
            """
            raise_errors_on_nested_writes('create', self, validated_data)

            ModelClass = self.Meta.model

            # Remove many-to-many relationships from validated_data.
            # They are not valid arguments to the default `.create()` method,
            # as they require that the instance has already been saved.
            info = model_meta.get_field_info(ModelClass)
            many_to_many = {}
            for field_name, relation_info in info.relations.items():
                if relation_info.to_many and (field_name in validated_data):
                    many_to_many[field_name] = validated_data.pop(field_name)

            try:
                instance = ModelClass(**validated_data)
                if user and not instance.has_save_permission(user):
                    raise exceptions.PermissionDenied()
                instance.save()
            except TypeError as exc:
                msg = (
                    'Got a `TypeError` when calling `%s.objects.create()`. '
                    'This may be because you have a writable field on the '
                    'serializer class that is not a valid argument to '
                    '`%s.objects.create()`. You may need to make the field '
                    'read-only, or override the %s.create() method to handle '
                    'this correctly.\nOriginal exception text was: %s.' %
                    (
                        ModelClass.__name__,
                        ModelClass.__name__,
                        self.__class__.__name__,
                        exc
                    )
                )
                raise TypeError(msg)

            # Save many-to-many relationships after the instance is created.
            if many_to_many:
                for field_name, value in many_to_many.items():
                    setattr(instance, field_name, value)

            return instance

        def update(self, instance, validated_data, user=None):
            raise_errors_on_nested_writes('update', self, validated_data)

            # Simply set each attribute on the instance, and then save it.
            # Note that unlike `.create()` we don't need to treat many-to-many
            # relationships as being a special case. During updates we already
            # have an instance pk for the relationships to be associated with.
            for attr, value in validated_data.items():
                setattr(instance, attr, value)
            if user and not instance.has_save_permission(user):
                raise exceptions.PermissionDenied()
            instance.save()

            return instance

    return SOS


def create_model_serializer(model_class, partial=True):
    serializer_class_getter = getattr(model_class, 'get_serializer_class', None)
    if serializer_class_getter is not None:
        serializer_class = serializer_class_getter()
    else:
        serializer_class = get_sync_object_serializer_class(model_class)
    return serializer_class(partial=partial)


class SyncObjectSerializer(serializers.ModelSerializer):
    model_name = serializers.CharField(max_length=201, min_length=3)

    serializer_related_field = SyncObjectRelatedField

    class Meta:
        model = SyncObject
        exclude = ('polymorphic_ctype',)
        read_only_fields = ('deleted',)

    def __init__(self, instance=None, data=fields.empty, **kwargs):
        # The following is needed for nicer HTML rendering. Side effects are not known.
        if isinstance(instance, SyncObject):
            self.Meta.model = instance.__class__
        else:
            self.Meta.model = SyncObject
            
        super(SyncObjectSerializer, self).__init__(instance, data, **kwargs)

    def create(self, validated_data):
        assert u'model_serializer' in validated_data
        s = validated_data.pop(u'model_serializer')
        return s.create(validated_data, user=getattr(self, 'user', None))

    def update(self, instance, validated_data):
        assert u'model_serializer' in validated_data
        s = validated_data.pop(u'model_serializer')
        return s.update(instance, validated_data, user=getattr(self, 'user', None))

    def to_internal_value(self, data):
        model_name = data.pop(u'model_name', None)
        if self.instance:
            s = create_model_serializer(self.instance.__class__)
        else:
            if model_name is None:
                raise exceptions.ValidationError(OrderedDict([(u'model_name', 'missing')]))
            try:
                model = apps.get_model(*model_name.split('.'))
            except Exception, e:
                raise exceptions.ValidationError(OrderedDict([(u'model_name', str(e))]))
                if not issubclass(model, SyncObject):
                    raise exceptions.ValidationError(
                        OrderedDict([(u'model_name', 'not a sync object')]))
            s = create_model_serializer(model)
        r = s.to_internal_value(data)
        r[u'model_serializer'] = s
        return r

    def to_representation(self, instance):
        assert isinstance(instance, SyncObject)
        if instance.deleted:
            return super(SyncObjectSerializer, self).to_representation(instance)
        s = create_model_serializer(instance.__class__)
        return s.to_representation(instance)
