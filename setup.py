# -*- coding: utf-8 -*-
import os
from distutils.core import setup


__author__ = 'Fedor Tyurin'
__version__ = '0.6.1'


os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name='django-rest-sync',
    version=__version__,
    packages=['sync', 'sync.auth', 'sync.migrations',
              'sync.management', 'sync.management.commands'],
    url='http://172.25.34.141/acando/sync',
    license='MIT',
    author=__author__,
    author_email='fedor.tyurin@acando.com',
    description='Django application for client synchronisation',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'Django>=1.8',
        'django-polymorphic==0.7.1',
        'djangorestframework>=3.1.1',
    ],
)
