from django.contrib import admin

from sync.admin import BaseSyncObjectAdmin

from . import models


@admin.register(models.Foo)
class FooAdmin(BaseSyncObjectAdmin):
    pass


@admin.register(models.Bar)
class BarAdmin(BaseSyncObjectAdmin):
    pass

