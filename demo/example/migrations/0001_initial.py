# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('sync', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bar',
            fields=[
                ('syncobject_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sync.SyncObject')),
                ('dt', models.DateTimeField(default=datetime.datetime(1999, 9, 1, 0, 0))),
            ],
            options={
                'abstract': False,
            },
            bases=('sync.syncobject',),
        ),
        migrations.CreateModel(
            name='Foo',
            fields=[
                ('syncobject_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sync.SyncObject')),
                ('i', models.IntegerField(default=1)),
                ('b', models.BooleanField(default=True)),
                ('s', models.CharField(default=b'abc', max_length=100)),
                ('t', models.TextField(default=b'emplty')),
            ],
            options={
                'abstract': False,
            },
            bases=('sync.syncobject',),
        ),
        migrations.AddField(
            model_name='bar',
            name='f',
            field=models.ForeignKey(blank=True, to='example.Foo', null=True),
        ),
    ]
