from datetime import datetime

from django.db import models

from sync.models import SyncObject


class Foo(SyncObject):
    i = models.IntegerField(default=1)
    b = models.BooleanField(default=True)
    s = models.CharField(max_length=100, default='abc')
    t = models.TextField(default='empty')


class Bar(SyncObject):
    f = models.ForeignKey('Foo', null=True, blank=True)
    dt = models.DateTimeField(default=datetime(1999, 9, 1))
